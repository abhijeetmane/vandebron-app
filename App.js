/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Provider } from 'react-redux';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import configureStore from './src/store/configureStore';
import Profile from './src/screens/Profile/Profile';
import UpdateProfile from './src/screens/UpdateProfile/UpdateProfile';

const store = configureStore();

const AppNavigator = createStackNavigator({
    Profile: Profile,
    UpdateProfile: UpdateProfile
}, {
    initialRouteName: 'Profile'
});

const Navigation = createAppContainer(AppNavigator);

const App: () => React$Node = () => {
    return (
        <Provider store={store}>
            <Navigation>
                <View style={styles.container}>
                    <Profile />
                </View>
            </Navigation>
        </Provider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
});

export default App;
