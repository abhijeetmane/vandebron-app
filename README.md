# Project Title

User Profile

# Project Description

This Project is to view and edit User profile.

### Tech Stack

Application uses a number of open source projects:

* [react-native]
* [redux]
* [ES6][JSX]
* [Jest]
* [Enzyme]
* [Node]

### Installation

Requires [Node.js](https://nodejs.org/) to run.

 -  ### For ios
    1. Make sure xcode is installed and emulators are available.
    2. cd vandebronApp
    3. npm install
    4. react-native run-ios
 -  ### For Android
    1. Make sure Android studio is installed and emulators are available.
    2. cd vandebronApp
    3. npm install
    4. react-native run-android

### Application Flow

##### Home Screen (Gegevens)

-  First screen of app where user profile is shown
- If user wants to update it then he can click on "Contactgegevens wijzigen" Button
- This action will take him to Edit screen
- Safe Area is considered for phones with notch.
-  App is responsive

##### Edit Screen (Contactgegevens wijzigen)

- User can update any field.
- Basic Validations are provided to all fields.
- Error messages are highlighed with red.
- Form is scrollable for smaller devices.
- Form is responsive
- If user does not want to edit anything, he can use back button to navigate back.
- After updating profile, user can click on "Contactgegevens bijwerken" Button to save changes
- User will be navigated to profile page where he can see updated profile page

### Unit Testing Coverage

```
$ cd vandebronApp
$ npm run test
```

### Ios Demo
![Image Ios Profile](./demo/ios.png)

### Android Demo
![Image Android Profile](./demo/android.png)