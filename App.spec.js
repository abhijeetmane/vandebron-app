import React from "react";
import Enzyme, { shallow } from "enzyme";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import App from "./App";
import thunk from 'redux-thunk';
import Adapter from 'enzyme-adapter-react-16'
Enzyme.configure({ adapter: new Adapter() });

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({ profile: {} })

jest.mock('react-navigation', () => {
    const createAppContainer = jest.fn();
    return { createAppContainer };
});

jest.mock('react-navigation-stack', () => {
    const createStackNavigator = jest.fn();
    return { createStackNavigator };
});

describe("App Component", () => {
    it("should render", () => {
        expect(
            shallow(
                <Provider store={store}>
                    <App />
                </Provider>
            )).toMatchSnapshot();
    });
});