module.exports = {
    preset: "react-native",
    transform: {
        "^.+\\.[t|j]sx?$": "babel-jest"
    },
    "transformIgnorePatterns": [
        "node_modules/(?!(jest-)?react-native|react-navigation|react-navigation-redux-helpers|react-native-gesture-handler|react-navigation-stack|@react-navigation/.*)"
    ],
    "setupFiles": ["./node_modules/react-native-gesture-handler/jestSetup.js"],
    "globals": {
        "window": {}
    }
    // transformIgnorePatterns: ["/node_modules/(?!sentry|react-native|react-navigation|react-native-gesture-handler).+\\.js$",]
};