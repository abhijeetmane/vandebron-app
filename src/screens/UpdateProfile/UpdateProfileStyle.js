import { StyleSheet } from 'react-native';
import COLORS from './../../styles/Colors';

export default StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 60
    },
    edit: {
        margin: 20,
        marginBottom: 0
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    edit_label: {
        fontSize: 16,
        fontWeight: "bold",
        lineHeight: 40
    },
    edit_value: {
        fontSize: 16,
        height: 50,
        backgroundColor: COLORS.silver,
        // lineHeight: 40,
        paddingLeft: 20,
    },
    save: {
        margin: 20,
    },
    save_btn: {
        flex: 1,
        width: "100%",
        height: 60,
        alignItems: "center",
        justifyContent: "space-around",
        flexDirection: "row",
        position: "absolute",
        bottom: 0,
        paddingLeft: 20,
        paddingRight: 20,
        fontWeight: "bold",
        backgroundColor: COLORS.navy
    },
    save_btn_label: {
        color: COLORS.white,
        fontSize: 18,
    },
    invalid: {
        borderColor: COLORS.error,
        borderWidth: 1,
    },
    disabled: {
        opacity: 0.5,
    }
});