import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from 'redux-thunk';
import { updateProfile } from '../../actions/index';
import UpdateProfile from "./UpdateProfile";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({ profile: {} });

beforeEach(() => {
    store.clearActions();
});

jest.mock('react-navigation', () => {
    const createAppContainer = jest.fn();
    return { createAppContainer };
});

Enzyme.configure({ adapter: new Adapter() })

describe("UpdateProfile Component", () => {
    it("should render", () => {
        expect(
            shallow(
                <Provider store={store}>
                    <UpdateProfile />
                </Provider>
            )).toMatchSnapshot();
    });

    it('Only SUCCESS actions type is returned on call of updateProfile Action', () => {
        store.dispatch(updateProfile({ name: "Johny" }));
        expect(store.getActions()[0].type).toBe("GET_PROFILE_SUCCESS");
        expect(store.getActions()).toMatchSnapshot();
    });

    it('PENDING actions type is not returned on call of updateProfile Action', () => {
        store.dispatch(updateProfile({ name: "Johny" }));
        expect(store.getActions()[0].type).not.toBe("GET_PROFILE_PENDING");
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Profile name should be updated to new name', () => {
        store.dispatch(updateProfile({ name: "Johny" }));
        expect(store.getActions()[0].data.name).toBe("Johny");
    });
});