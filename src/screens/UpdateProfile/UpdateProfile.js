import React, { Component } from 'react';
import { View, Text, TextInput, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import { updateProfile } from '../../actions/index';
import Header from '../../components/Header/Header';
import Error from '../../components/Error/Error';
import { validateField, isFormValid } from '../../components/validation/validation';
import styles from './UpdateProfileStyle';

class UpdateProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: null,
            emailId: null,
            postcode: null,
            house: null,
            addition: null,
            street: null,
            city: null,
            formValid: 'true',
            emailValid: 'true',
            phoneValid: 'true',
            postcodeValid: 'true',
            houseValid: 'true',
            streetValid: 'true',
            cityValid: 'true'

        };
    }

    componentDidMount() {
        const { name, userId, phone, emailId, postcode, house, addition, street, city } = this.props.profile;
        this.setState({
            name, userId, phone, emailId, postcode, house, addition, street, city
        });
    }

    static navigationOptions = {
        headerTitle: <Header title="Contactgegevens wijzigen" />,
    };

    validateForm = () => {
        const { emailValid, phoneValid, postcodeValid, cityValid, streetValid, houseValid } = this.state;
        const formValid = isFormValid(emailValid, phoneValid, postcodeValid, cityValid, streetValid, houseValid);
        this.setState({ formValid });
    }

    validateItem = (name, value, fieldName = false) => {
        if (fieldName) {
            const fieldValid = validateField(name, value);
            this.setState({ [fieldName]: fieldValid }, this.validateForm);
        }
    }

    handleUserInput = (name, value, fieldName) => {
        this.setState({ [name]: value },
            () => { this.validateItem(name, value, fieldName) });
    }

    updateProfile = () => {
        const { name, userId, phone, emailId, postcode, house, addition, street, city } = this.state;
        this.props.updateProfile({ name, userId, phone, emailId, postcode, house, addition, street, city });
        this.props.navigation.navigate('Profile');
    }

    goToProfileScreen = () => {
        const backAction = NavigationActions.back();
        this.props.navigation.dispatch(backAction);
    }

    render() {
        const profile = this.state;
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={styles.container} keyboardDismissMode='on-drag' keyboardShouldPersistTaps='handled'>
                    <View style={styles.edit}>
                        <Text style={styles.edit_label}>Telefoonnummer</Text>
                        <TextInput value={profile.phone}
                            onChangeText={(value) => this.handleUserInput('phone', value, 'phoneValid')}
                            style={[styles.edit_value, profile.phoneValid == 'false' && styles.invalid]} keyboardType='numeric'></TextInput>
                        {profile.phoneValid == 'false' && <Error type="phone" />}
                    </View>
                    <View style={styles.edit}>
                        <Text style={styles.edit_label}>E-mailadres</Text>
                        <TextInput value={profile.emailId} name="email"
                            onChangeText={(value) => this.handleUserInput('emailId', value, 'emailValid')}
                            style={[styles.edit_value, profile.emailValid == 'false' && styles.invalid]} keyboardType='email-address'></TextInput>
                        {profile.emailValid == 'false' && <Error type="emailId" />}
                    </View>
                    <View style={styles.edit}>
                        <Text style={styles.edit_label}>Postcode</Text>
                        <TextInput value={profile.postcode}
                            onChangeText={(value) => this.handleUserInput('postcode', value, 'postcodeValid')}
                            style={[styles.edit_value, profile.postcodeValid == 'false' && styles.invalid]}></TextInput>
                        {profile.postcodeValid == 'false' && <Error type="postcode" />}
                    </View>
                    <View style={[styles.edit, styles.row]}>
                        <View style={{ flex: 0.4 }}>
                            <Text style={styles.edit_label}>Huisnummer</Text>
                            <TextInput value={profile.house}
                                onChangeText={(value) => this.handleUserInput('house', value, 'houseValid')}
                                style={[styles.edit_value, profile.houseValid == 'false' && styles.invalid]} keyboardType='numeric'></TextInput>
                            {profile.houseValid == 'false' && <Error type="house" />}
                        </View>
                        <View style={{ flex: 0.4 }}>
                            <Text style={styles.edit_label}>Toevoeging</Text>
                            <TextInput value={profile.addition}
                                onChangeText={(value) => this.handleUserInput('addition', value)}
                                style={styles.edit_value}></TextInput>
                        </View>
                    </View>
                    <View style={styles.edit}>
                        <Text style={styles.edit_label}>Straat</Text>
                        <TextInput value={profile.street}
                            onChangeText={(value) => this.handleUserInput('street', value, 'streetValid')}
                            style={[styles.edit_value, profile.streetValid == 'false' && styles.invalid]}></TextInput>
                        {profile.streetValid == 'false' && <Error type="street" />}
                    </View>
                    <View style={styles.edit}>
                        <Text style={styles.edit_label}>Plaats</Text>
                        <TextInput value={profile.city}
                            onChangeText={(value) => this.handleUserInput('city', value, 'cityValid')}
                            style={[styles.edit_value, profile.cityValid == 'false' && styles.invalid]}></TextInput>
                        {profile.cityValid == 'false' && <Error type="city" />}
                    </View>
                </ScrollView>
                <View style={styles.save}>
                    <TouchableOpacity onPress={this.updateProfile}
                        style={[styles.save_btn, profile.formValid == 'false' && styles.disabled]} disabled={profile.formValid == 'false'}>
                        <Text style={styles.save_btn_label}>Contactgegevens bijwerken</Text>
                        <Icon name="md-arrow-forward" size={24} color="#fff" />
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = state => {
    return {
        profile: state.profile.data,
    };
};

export default connect(mapStateToProps, { updateProfile })(UpdateProfile);
