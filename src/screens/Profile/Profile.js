import React, { Component } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import { getProfile } from '../../actions/index';
import Header from '../../components/Header/Header';
import styles from './profileStyle';

class Profile extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.getProfile();
    }

    static navigationOptions = {
        headerTitle: <Header title="Gegevens" />
    };

    updateProfile = () => {
        this.props.navigation.navigate('UpdateProfile');
    }

    render() {
        const { name, userId, phone, emailId, postcode, house, addition, street, city } = this.props.profile;
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    <View style={styles.contact}>
                        <Text style={styles.contact_name}>{name}</Text>
                        <Text style={styles.contact_number}>{userId}</Text>
                    </View>
                    <View style={styles.contact_details}>
                        <Text style={styles.contact_details_title}>Contactgegevens</Text>
                        <Text style={styles.contact_details_value}>{phone}</Text>
                        <Text style={styles.contact_details_value}>{emailId}</Text>
                        <Text style={styles.contact_details_value}>{street} {house} {addition}</Text>
                        <Text style={styles.contact_details_value}>{postcode} {city}</Text>
                    </View>
                    <TouchableOpacity onPress={this.updateProfile} style={styles.contact_edit_btn}>
                        <Text style={styles.contact_edit_btn_label}>Contactgegevens wijzigen</Text>
                        <Icon name="md-arrow-forward" size={30} color="#000" />
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = state => {
    return {
        profile: state.profile.data,
    };
};

export default connect(mapStateToProps, { getProfile })(Profile);