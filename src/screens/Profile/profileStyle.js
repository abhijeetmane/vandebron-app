import { StyleSheet } from 'react-native';
import COLORS from './../../styles/Colors';

export default StyleSheet.create({
    container: {
        flex: 1,
    }, contact: {
        flex: 1,
        height: 100,
        alignItems: "flex-start",
        justifyContent: "center",
        paddingLeft: 20,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: COLORS.shadow,
        shadowColor: COLORS.shadow,
        backgroundColor: COLORS.white,
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 6,
    },
    contact_name: {
        fontWeight: "bold",
        fontSize: 34,
    },
    contact_number: {
        fontSize: 20,
        color: COLORS.gray
    },
    contact_details: {
        flex: 4,
        justifyContent: "center",
        alignItems: "flex-start",
        paddingLeft: 20,
    },
    contact_details_title: {
        fontWeight: "bold",
        fontSize: 24,
        marginBottom: 10
    },
    contact_details_value: {
        fontSize: 18,
        lineHeight: 50,
        color: COLORS.gray
    },
    contact_edit_btn: {
        flex: 0.5,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingRight: 20,
        paddingLeft: 20,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: COLORS.shadow,
        shadowColor: COLORS.shadow,
        backgroundColor: COLORS.white,
        shadowOffset: { width: 5, height: -3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 6,
    },
    contact_edit_btn_label: {
        color: COLORS.black,
        fontSize: 24,
    }
});