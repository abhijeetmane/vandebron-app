import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from 'redux-thunk';
import { getProfile } from '../../actions/index';
import Profile from "./Profile";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({ profile: {} });

beforeEach(() => {
    store.clearActions();
});

jest.mock('react-navigation', () => {
    const createAppContainer = jest.fn();
    return { createAppContainer };
});

Enzyme.configure({ adapter: new Adapter() })

describe("Profile Component", () => {
    it("should render", () => {
        expect(
            shallow(
                <Provider store={store}>
                    <Profile />
                </Provider>
            )).toMatchSnapshot();
    });

    it('PENDING and SUCCESS actions types are returned on call of getProfile Action', () => {
        store.dispatch(getProfile());
        expect(store.getActions()[0].type).toBe("GET_PROFILE_PENDING");
        expect(store.getActions()[1].type).toBe("GET_PROFILE_SUCCESS");
        expect(store.getActions()).toMatchSnapshot();
    });

    it('Profile name should be correct', () => {
        store.dispatch(getProfile());
        expect(store.getActions()[1].data.name).toBe("John Doe");
    });
});