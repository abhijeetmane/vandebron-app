import {
  GET_PROFILE_SUCCESS,
  GET_PROFILE_PENDING,
  GET_PROFILE_FAILED,
} from '../actions/actionTypes';

const initialState = {
  data: [],
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROFILE_SUCCESS:
      return {
        ...state,
        data: action.data,
        loading: false,
        failed: false,
      };
    case GET_PROFILE_PENDING:
      return {
        ...state,
        data: action.data,
        loading: true,
        failed: false,
      };
    case GET_PROFILE_FAILED:
      return {
        ...state,
        data: action.data,
        loading: false,
        failed: true,
      };
    default:
      return state;
  }
};

export default profileReducer;
