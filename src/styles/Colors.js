export default COLORS = {
    white: '#fff',
    black: '#000',
    shadow: '#ddd',
    navy: '#16202e',
    silver: '#ededed',
    gray: '#808080',
    error: '#FF0000'
}