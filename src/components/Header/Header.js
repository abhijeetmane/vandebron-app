import React from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';

export default function Header(props) {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" hidden={false} backgroundColor="#fff" translucent={false} />
            <Text style={styles.header_title}>{props.title}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
    },
    header_title: {
        fontSize: 20,
        fontWeight: "bold",
    }
});