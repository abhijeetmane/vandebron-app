export function validateField(name, value, fieldName) {
    let fieldValid = true;
    switch (name) {
        case 'emailId':
            fieldValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/gi) ? 'true' : 'false';
            break;
        case 'phone':
            fieldValid = value.match(/^[0-9]*$/gi) ? String(value.length == 10) : 'false';
            break;
        default:
            fieldValid = isEmpty(value)
            break;
    }
    return fieldValid;
}


export function isFormValid(...fields) {
    const formValid = fields.some((field) => field == 'false');
    return String(!formValid);
}

function isEmpty(value) {
    return value ? String(value.length > 0) : 'false';
}