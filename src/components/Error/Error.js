import React from 'react';
import { Text, StyleSheet } from 'react-native';
import Colors from '../../styles/Colors';

const errors = {
    'phone': 'Telefoonnummer is not valid.',
    'emailId': 'E-mailadres is not valid.',
    'postcode': 'Postcode is not valid',
    'house': 'Huisnummer is not valid',
    'street': 'Straat is not valid',
    'city': 'Plaats is not valid'
}
export default function Error(props) {
    return <Text style={styles.error_title}>{errors[props.type]}</Text>;
}

const styles = StyleSheet.create({
    error_title: {
        fontSize: 14,
        fontWeight: "500",
        color: Colors.error,
    }
});