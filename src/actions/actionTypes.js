export const GET_PROFILE_SUCCESS = 'GET_PROFILE_SUCCESS';
export const GET_PROFILE_PENDING = 'GET_PROFILE_PENDING';
export const GET_PROFILE_FAILED = 'GET_PROFILE_FAILED';