import {
  GET_PROFILE_SUCCESS,
  GET_PROFILE_PENDING,
  GET_PROFILE_FAILED
} from './actionTypes';

const API_ENDPOINT = 'http://demo7239897.mockable.io/profile';

/** Use mock in case API is not reachable */
const profileMock = {
  "name": "John Doe",
  "userId": "13619839173",
  "phone": "0622609703",
  "emailId": "john@due.com",
  "street": "Herengracht",
  "city": "Amsterdam",
  "house": "545",
  "addition": "Rood",
  "postcode": "1017BW"
}

const getProfileSuccess = data => {
  return {
    type: GET_PROFILE_SUCCESS,
    data,
  };
};

function getProfilePending(data) {
  return {
    type: GET_PROFILE_PENDING,
    data,
  };
}

function getProfileFailed(data) {
  return {
    type: GET_PROFILE_FAILED,
    data,
  };
}

const fetchProfile = async () => {
  const response = await fetch(API_ENDPOINT, {
    method: 'GET',
  });
  const data = await response.json();
  return data;
};

const getProfile = () => async dispatch => {
  dispatch(getProfilePending('Loading'));
  try {
    // const profileMock = await fetchProfile();
    dispatch(getProfileSuccess(profileMock));
  } catch (e) {
    dispatch(getProfileFailed(e));
  }
};

const updateProfile = profile => async dispatch => {
  dispatch(getProfileSuccess(profile));
};

export {
  getProfile,
  updateProfile,
};